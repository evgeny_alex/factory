import com.evgeny_alex.config.ApplicationConfig;
import com.evgeny_alex.controller.StatisticController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
@WebMvcTest(StatisticController.class)
public class StatisticControllerIntegrationTest {

    /** Поле mock, для тестирования HTTP-запросов */
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void statisticTest() throws Exception {
        mockMvc.perform(post("/supply")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"carName\": \"MERCEDES\",\n" +
                        "  \"detailName\": \"ENGINE\"\n" +
                        "}"))
                .andExpect(status().isCreated());

        mockMvc.perform(post("/supply")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"carName\": \"MERCEDES\",\n" +
                        "  \"detailName\": \"ACCESSORY\"\n" +
                        "}"))
                .andExpect(status().isCreated());

        mockMvc.perform(post("/supply")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"carName\": \"MERCEDES\",\n" +
                        "  \"detailName\": \"BODY\"\n" +
                        "}"))
                .andExpect(status().isCreated());

        Thread.sleep(1000);

        mockMvc.perform(get("/order?name=mercedes&count=1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[{\"detailList\":[{\"carName\":\"MERCEDES\",\"detailName\":\"ACCESSORY\"},{\"carName\":\"MERCEDES\",\"detailName\":\"BODY\"},{\"carName\":\"MERCEDES\",\"detailName\":\"ENGINE\"}],\"carName\":\"MERCEDES\"}]"));

        mockMvc.perform(get("/statistic"))
                .andExpect(status().isOk())
                .andExpect(content().string("All : 1; current : 0; completed : 1."));
    }
}
