import com.evgeny_alex.config.ApplicationConfig;
import com.evgeny_alex.controller.DetailController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
@WebMvcTest(DetailController.class)
public class DetailControllerIntegrationTest {

    /** Поле mock, для тестирования HTTP-запросов */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Тест посталяет двигатель на склад и получает статус 201 (Created).
     */
    @Test
    public void supplyDetailEngineTest() throws Exception {
        mockMvc.perform(post("/supply")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"carName\": \"TOYOTA\",\n" +
                        "  \"detailName\": \"ENGINE\"\n" +
                        "}"))
                .andExpect(status().isCreated());
    }

    /**
     * Тест посталяет аксессуар на склад и получает статус 201 (Created).
     */
    @Test
    public void supplyDetailAccessoryTest() throws Exception {
        mockMvc.perform(post("/supply")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"carName\": \"FORD\",\n" +
                        "  \"detailName\": \"ACCESSORY\"\n" +
                        "}"))
                .andExpect(status().isCreated());
    }

    /**
     * Тест посталяет кузов на склад и получает статус 201 (Created).
     */
    @Test
    public void supplyDetailBodyTest() throws Exception {
        mockMvc.perform(post("/supply")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"carName\": \"MERCEDES\",\n" +
                        "  \"detailName\": \"BODY\"\n" +
                        "}"))
                .andExpect(status().isCreated());
    }

}
