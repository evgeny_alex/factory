import com.evgeny_alex.config.ApplicationConfig;
import com.evgeny_alex.controller.CarController;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Интеграционные тесты.
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ApplicationConfig.class)
@WebMvcTest(CarController.class)
public class CarControllerIntegrationTest {

    /** Поле mock, для тестирования HTTP-запросов */
    @Autowired
    private MockMvc mockMvc;

    /**
     * Тест посталяет детили на склад, запрашивает машину и получает статус 200 (Ok).
     * Добавлен Thread.sleep(), потому что необходимо чтобы компания произвела машину.
     * Иначе на складе не будет этой машину и сервер вернет ответ 403 (Forbidden).
     */
    @Test
    public void supplyDetailAndGetCarTest() throws Exception {
        mockMvc.perform(post("/supply")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"carName\": \"MERCEDES\",\n" +
                        "  \"detailName\": \"ENGINE\"\n" +
                        "}"))
                .andExpect(status().isCreated());

        mockMvc.perform(post("/supply")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"carName\": \"MERCEDES\",\n" +
                        "  \"detailName\": \"BODY\"\n" +
                        "}"))
                .andExpect(status().isCreated());

        mockMvc.perform(post("/supply")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "  \"carName\": \"MERCEDES\",\n" +
                        "  \"detailName\": \"ACCESSORY\"\n" +
                        "}"))
                .andExpect(status().isCreated());

        //Thread.sleep(1000);

        mockMvc.perform(get("/order?name=mercedes&count=1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[{\"detailList\":[{\"carName\":\"MERCEDES\",\"detailName\":\"ACCESSORY\"},{\"carName\":\"MERCEDES\",\"detailName\":\"BODY\"},{\"carName\":\"MERCEDES\",\"detailName\":\"ENGINE\"}],\"carName\":\"MERCEDES\"}]"));
    }

    /**
     * Тест эммитирует параллельную работу трех поставщиков и заказчиков.
     */
    @Test
    public void someSuppliersAndCustomersTest() {
        new Thread(new SupplierAndCustomer()).start();
        new Thread(new SupplierAndCustomer()).start();
        new Thread(new SupplierAndCustomer()).start();
    }

    /**
     * Класс эммитирует работу одного поставщика и заказчика.
     */
    class SupplierAndCustomer implements Runnable {

        @SneakyThrows
        @Override
        public void run() {
            for (int i = 0; i < 3; i++) {
                mockMvc.perform(post("/supply")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"carName\": \"MERCEDES\",\n" +
                                "  \"detailName\": \"ENGINE\"\n" +
                                "}"))
                        .andExpect(status().isCreated());

                mockMvc.perform(post("/supply")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"carName\": \"MERCEDES\",\n" +
                                "  \"detailName\": \"BODY\"\n" +
                                "}"))
                        .andExpect(status().isCreated());

                mockMvc.perform(post("/supply")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"carName\": \"MERCEDES\",\n" +
                                "  \"detailName\": \"ACCESSORY\"\n" +
                                "}"))
                        .andExpect(status().isCreated());

                //Thread.sleep(1000);

                mockMvc.perform(get("/order?name=mercedes&count=1"))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                        .andExpect(content().json( "[{\"detailList\":[{\"carName\":\"MERCEDES\",\"detailName\":\"ENGINE\"},{\"carName\":\"MERCEDES\",\"detailName\":\"BODY\"},{\"carName\":\"MERCEDES\",\"detailName\":\"ACCESSORY\"}],\"carName\":\"MERCEDES\"}]"));
            }
        }
    }
}
