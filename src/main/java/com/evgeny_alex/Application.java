package com.evgeny_alex;

import com.evgeny_alex.config.ApplicationConfig;
import org.springframework.boot.SpringApplication;

/**
 * Класс запуска приложения.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
public class Application {

    /**
     * Метод main, начало программы.
     *
     * @param args - параметры командной строки
     */
    public static void main(String[] args) {
        SpringApplication.run(ApplicationConfig.class, args);
    }
}
