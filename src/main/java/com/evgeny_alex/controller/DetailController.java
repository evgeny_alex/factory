package com.evgeny_alex.controller;

import lombok.AllArgsConstructor;
import com.evgeny_alex.company.product.detail.Detail;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.evgeny_alex.service.DetailService;

import javax.validation.Valid;

/**
 * Класс-контроллер, который принимает детали на склады.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@RestController
@AllArgsConstructor
public class DetailController {

    private final DetailService detailService;

    /**
     * Метод, который обрабатывает POST-запросы по адресу /supply.
     * Производитель указывает деталь и по этому адресу отправляет ее в компанию.
     *
     * @param detail - деталь, которую производитель поставляет в компанию.
     */
    @PostMapping(value = "/supply")
    @ResponseStatus(HttpStatus.CREATED)
    public void supply(@Valid @RequestBody Detail detail) {
        detailService.putDetail(detail);
    }
}
