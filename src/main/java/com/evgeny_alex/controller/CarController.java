package com.evgeny_alex.controller;

import lombok.AllArgsConstructor;
import com.evgeny_alex.company.product.car.Car;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.evgeny_alex.service.CarService;

import java.util.List;

/**
 * Класс-контроллер, который обрабатывает заказы на машины.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@RestController
@AllArgsConstructor
public class CarController {

    private CarService carService;

    /**
     * Метод, который обрабатывает GET-запросы по адресу /order.
     * Покупатель может указать название машины и ее количество для заказа.
     *
     * @param name  - название машины
     * @param count - нужное количество машин
     * @return - лист, этих машин. Если таких нет - возвращается пустой лист.
     */
    @GetMapping(value = "/order")
    @ResponseStatus(HttpStatus.OK)
    public List<Car> order(@RequestParam("name") String name, @RequestParam("count") int count) {
        return carService.getCars(name, count);
    }
}
