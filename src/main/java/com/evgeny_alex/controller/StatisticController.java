package com.evgeny_alex.controller;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.evgeny_alex.service.StatisticService;

/**
 * Класс-котроллер, который обрабатывает запросы по выводу статистики.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@RestController
@AllArgsConstructor
public class StatisticController {

    private final StatisticService statisticService;

    /**
     * Метод, который запрашивает статистику заказов.
     */
    @GetMapping(value = "/statistic")
    @ResponseStatus(HttpStatus.OK)
    public String getStatistic() {
        return statisticService.getStatistic();
    }
}
