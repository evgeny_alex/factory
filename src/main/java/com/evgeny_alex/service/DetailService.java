package com.evgeny_alex.service;

import com.evgeny_alex.company.factory.Factory;
import com.evgeny_alex.company.product.car.CarEnum;
import com.evgeny_alex.company.product.detail.Detail;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class DetailService {

    /** Map в котором хранятся подфабрики всех продуктов */
    @Resource(name = "factoryMap")
    private Map<CarEnum, Factory> factoryMap;

    /**
     * Метод пробует произвести поставку продукта
     * @param detail - деталь, поставляемая производителем
     */
    public void putDetail(Detail detail) {

        Factory factory = factoryMap.get(detail.getCarName());

        factory.putDetail(detail);
    }
}
