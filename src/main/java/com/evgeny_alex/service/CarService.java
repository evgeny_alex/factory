package com.evgeny_alex.service;

import com.evgeny_alex.company.factory.Factory;
import com.evgeny_alex.company.product.car.Car;
import com.evgeny_alex.company.product.car.CarEnum;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class CarService {

    /** Map в котором хранятся фабрики всех продуктов */
    @Resource(name = "factoryMap")
    private Map<CarEnum, Factory> factoryMap;

    private StatisticService statisticService;

    /**
     * Метод пробует получить продукт и возвращает ответ.
     * Обращаемся к сервису статистики, чтобы собирал и обрабатывал статистику
     *
     * @param name  - название машины
     * @param count - количество машин
     * @return если такие машины есть, возвращаем лист машин, иначе - пустой лист
     */
    public List<Car> getCars(String name, int count) {
        statisticService.startOrder();

        CarEnum carEnum = CarEnum.findByName(name);

        Factory factory = factoryMap.get(carEnum);

        List<Car> carList = factory.getCars(count);

        carList.forEach(car -> car.setCarName(carEnum));

        statisticService.endOrder(carList);

        return carList;
    }

}
