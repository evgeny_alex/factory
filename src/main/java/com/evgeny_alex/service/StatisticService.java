package com.evgeny_alex.service;

import com.evgeny_alex.company.product.car.Car;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class StatisticService {

    /** Количество всех заказов */
    private AtomicInteger countAllOrder = new AtomicInteger(0);
    /** Количество всех заказов */
    private AtomicInteger countCurrentOrder = new AtomicInteger(0);
    /** Количество всех заказов */
    private AtomicInteger countCompletedOrder = new AtomicInteger(0);

    /**
     * Метод возвращает статистику компании.
     */
    public String getStatistic() {
        return "All : " + countAllOrder + "; current : " + countCurrentOrder + "; completed : " + countCompletedOrder + ".";
    }

    /**
     * Метод обрабатывает начало заказа.
     */
    void startOrder() {
        countAllOrder.incrementAndGet();

        countCurrentOrder.incrementAndGet();
    }


    /**
     * Метод обрабатывает завершение заказа.
     * Если лист непустой, увеличиваем счетчик успешных заказов.
     *
     * @param carList - лист машин, ответ на заказ
     */
    void endOrder(List<Car> carList) {
        countCurrentOrder.decrementAndGet();

        if (!carList.isEmpty()) {
            countCompletedOrder.incrementAndGet();
        }
    }
}
