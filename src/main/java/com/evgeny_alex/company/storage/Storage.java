package com.evgeny_alex.company.storage;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * Класс generic, который представляет хранилище для машин и всех деталей.
 * Является бином со scope prototype.
 *
 * @param <T> - параметр generic-класса, для класса Car и Detail
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@Component
@Scope("prototype")
public class Storage<T> {

    private static final Logger log = Logger.getLogger(Storage.class);

    /** Максимальный размер хранилища */
    private static final int SIZE = 20;

    /** Очередь, в которой хранятся объекты хранилища */
    private ConcurrentLinkedDeque<T> list = new ConcurrentLinkedDeque<>();

    /**
     * Метод который забирает объект со склада.
     *
     * @return возвращает объект хранения
     */
    public synchronized T get() {

        while (list.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException ignored) {
            }
        }
        T object = list.pop();
        notify();

        log.info("Used " + object.toString() + ". Object on storage: " + list.size());

        return object;
    }

    /**
     * Метод который кладет объект на склад.
     *
     * @param object - объект, который поставляется на склад
     */
    public synchronized void put(T object) {
        while (list.size() >= SIZE) {
            try {
                wait();
            } catch (InterruptedException ignored) {
            }
        }
        list.add(object);

        log.info("Added " + object.toString() + ". Object on storage: " + list.size());

        notify();
    }

    /**
     * Метод возвращает размер хранилища
     *
     * @return размер хранилища
     */
    public synchronized int getStorageSize() {
        return list.size();
    }
}
