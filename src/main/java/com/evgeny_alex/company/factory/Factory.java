package com.evgeny_alex.company.factory;

import com.evgeny_alex.company.product.car.Car;
import com.evgeny_alex.company.product.detail.Detail;
import com.evgeny_alex.company.product.detail.DetailEnum;
import com.evgeny_alex.company.storage.Storage;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Класс фабрик по производству машин.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@Component
@Scope("prototype")
@AllArgsConstructor
public class Factory implements Runnable {

    private static final Logger log = Logger.getLogger(Factory.class);

    //** Хранилище готовых машин. Внедряется prototype bean Storage<Car> */
    private Storage<Car> storageCar;
    //** Хранилище деталей. Внедряется bean Map из конфигурации */
    private Map<DetailEnum, Storage<Detail>> storageMap;

    /**
     * Метод, забирает готовую машину/машины.
     *
     * @param count - количество, запрашиваемых машин
     * @return лист машин
     */
    public List<Car> getCars(int count) {

        List<Car> list = new ArrayList<>();

        if (storageCar.getStorageSize() < count) {

            log.error("Order on " + count + " cars not completed.");

            return list;

        }

        for (int i = 0; i < count; i++) {
            list.add(storageCar.get());
        }

        log.info("Order on " + count + " cars completed.");

        return list;
    }

    /**
     * Метод кладет деталь на склад.
     *
     * @param detail - деталь, которая поставляется производителем на склад
     */
    public void putDetail(Detail detail) {

        DetailEnum detailName = detail.getDetailName();

        Storage<Detail> storage = storageMap.get(detailName);

        storage.put(detail);

        log.info("The item was delivered to the storage - " + detailName + " for the car " + detail.getCarName());
    }


    /**
     * Метод run (интерфейса Runnable), берет детали со склада и
     * собирает из них машину, которую потом кладет на склад.
     */
    @SneakyThrows
    @Override
    public void run() {

        while (true) {
            List<Detail> list = new ArrayList<>();

            storageMap.forEach((key, storage) -> list.add(storage.get()));

            storageCar.put(new Car(list));
        }
    }
}