package com.evgeny_alex.company.product.detail;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public enum DetailEnum {

    ENGINE("engine"),

    BODY("body"),

    ACCESSORY("accessory");

    private final String name;

}
