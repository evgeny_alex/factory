package com.evgeny_alex.company.product.detail;

import com.evgeny_alex.company.product.car.CarEnum;
import lombok.Data;

/**
 * Класс, которы представляет деталь.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@Data
public class Detail {

    /** Название машины, к которой относится деталь */
    private CarEnum carName;

    /** Название детали */
    private DetailEnum detailName;

}
