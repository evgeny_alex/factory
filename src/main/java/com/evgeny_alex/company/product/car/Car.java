package com.evgeny_alex.company.product.car;

import com.evgeny_alex.company.product.detail.Detail;
import lombok.Data;

import java.util.List;

/**
 * Класс, который описывает объект машины.
 *
 * @author Evgeny Aleksandrov
 * @version 1.0
 */
@Data
public class Car {

    /** Лист деталей, из которых состоит машина */
    private List<Detail> detailList;

    /** Название машины, перечисление */
    protected CarEnum carName;

    /**
     * Конструктор - создание новой машины, с ее составляющими.
     *
     * @param list - аксессуар
     */
    public Car(List<Detail> list) {
        this.detailList = list;
    }
}
