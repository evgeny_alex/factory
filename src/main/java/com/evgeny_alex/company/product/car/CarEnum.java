package com.evgeny_alex.company.product.car;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;


/**
 * Перечисление, которое описывает название машин.
 */
@Getter
@ToString
@AllArgsConstructor
public enum CarEnum {

    TOYOTA("toyota"),

    FORD("ford"),

    MERCEDES("mercedes");

    private final String name;

    /**
     * Метод находит по строке нужный объект enum.
     */
    public static CarEnum findByName(String name) {

        for (CarEnum value : CarEnum.values()) {
            if (value.getName().equals(name)) {
                return value;
            }
        }

        return null;
    }

}
