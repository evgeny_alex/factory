package com.evgeny_alex.config;

import com.evgeny_alex.company.factory.Factory;
import com.evgeny_alex.company.product.car.CarEnum;
import com.evgeny_alex.company.product.detail.Detail;
import com.evgeny_alex.company.product.detail.DetailEnum;
import com.evgeny_alex.company.storage.Storage;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.evgeny_alex.controller", "com.evgeny_alex.service", "com.evgeny_alex.company.factory", "com.evgeny_alex.company.storage"})
public class ApplicationConfig {

    /**
     * Метод создает бины storageMap (prototype) для хранения деталей для каждой Factory.
     *
     * @param storageAccessory - хранилище аксессуаров
     * @param storageBody - хранилище кузовов
     * @param storageEngine - хранилище двигателей
     * @return - карта хранилищ деталей
     */
    @Bean
    @Scope("prototype")
    public Map<DetailEnum, Storage<Detail>> storageMap(Storage<Detail> storageAccessory, Storage<Detail> storageBody, Storage<Detail> storageEngine) {
        Map<DetailEnum, Storage<Detail>> map = new HashMap<>();

        map.put(DetailEnum.ACCESSORY, storageAccessory);
        map.put(DetailEnum.BODY, storageBody);
        map.put(DetailEnum.ENGINE, storageEngine);

        return map;
    }


    /**
     * Метод создает бин factoryMap (singleton), в Map хранятся фабрики всех автомобилей.
     *
     * @param factoryToyota - фабрика toyota
     * @param factoryMercedes - фабрика mercedes
     * @param factoryFord - фабрика ford
     * @return - карта фабрик
     */
    @Bean
    public Map<CarEnum, Factory> factoryMap(Factory factoryToyota, Factory factoryMercedes, Factory factoryFord) {

        Map<CarEnum, Factory> map = new HashMap<>();

        map.put(CarEnum.TOYOTA, factoryToyota);
        map.put(CarEnum.FORD, factoryFord);
        map.put(CarEnum.MERCEDES, factoryMercedes);

        for (Map.Entry e : map.entrySet()) {
            Factory factory = (Factory) e.getValue();
            new Thread(factory).start();
        }

        return map;
    }
}
